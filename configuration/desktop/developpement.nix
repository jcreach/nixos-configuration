{ config, lib, ... }:

let inherit (import ../../utilities/scanner.nix { inherit lib; }) importNixFolder;
in
{
  imports = importNixFolder ./developpement;
}
