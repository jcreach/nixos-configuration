{ config, pkgs, lib, ... }:

{
    # Enable rdp
    services.xrdp.enable = true;

    # chose windows manager for rdp
    services.xrdp.defaultWindowManager = "gnome";
    networking.firewall.allowedTCPPorts = [ 3389 ];
}
