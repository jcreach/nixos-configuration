{ config, pkgs, lib, ... }:

{
  # Enable NVIDIA driver
  services.xserver.videoDrivers = [ "nvidia" ];
}
