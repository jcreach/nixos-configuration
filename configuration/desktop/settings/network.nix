{ config, pkgs, lib, ... }:

{
    networking.networkmanager.enable = true;
    networking.hostName = "jcreachDesktop"; # Define your hostname.
    networking.hostId = "749fc9aa";

    # The global useDHCP flag is deprecated, therefore explicitly set to false here.
    # Per-interface useDHCP will be mandatory in the future, so this generated config
    # replicates the default behaviour.
    networking.useDHCP = false;
    networking.interfaces.enp0s31f6.useDHCP = true;
    networking.interfaces.wlp5s0.useDHCP = true;
}
