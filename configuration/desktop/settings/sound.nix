{ config, pkgs, lib, ... }:

{
    # Enable sound.
    sound.enable = true;
    hardware.pulseaudio.enable = true;
}
