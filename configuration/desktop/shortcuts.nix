{ config, pkgs, lib, ... }:

let
  braveApp = url: "${pkgs.brave}/bin/brave --noerrdialogs --no-first-run --app=${url}";

  # Youtube
  youtube = pkgs.makeDesktopItem {
    name = "youtube";
    exec = braveApp "https://youtube.fr";
    desktopName = "YouTube";
    categories = "AudioVideo";
    icon = (builtins.fetchurl "https://icons.iconarchive.com/icons/dakirby309/simply-styled/256/YouTube-icon.png");
  };

  # Netflix
  netflix = pkgs.makeDesktopItem {
    name = "netflix";
    exec = braveApp "https://netflix.com";
    desktopName = "Netflix";
    categories = "AudioVideo";
    icon = (builtins.fetchurl "https://iconarchive.com/download/i106070/papirus-team/papirus-apps/netflix.svg");
  };

  # Disney+
  disneyplus = pkgs.makeDesktopItem {
    name = "disneyplus";
    exec = braveApp "https://www.disneyplus.com/";
    desktopName = "Disney+";
    categories = "AudioVideo";
    icon = (builtins.fetchurl "https://seeklogo.com/images/D/disney-logo-575AED0F1D-seeklogo.com.png");
  };

  # NixOS Configuration
  configEdition = pkgs.makeDesktopItem {
    name = "nixos_config";
    exec = "codium /etc/nixos/nixos-configuration";
    comment = "Open the NixOS configuration with Codium";
    desktopName = "NixOS Config";
    genericName = "Configuration";
    categories = "Development";
    icon = (builtins.fetchurl "https://nixos.org/logo/nixos-logo-only-hires.png");
  };
in
{
  # Packages
  environment.systemPackages = with pkgs; [
    youtube
    netflix
    disneyplus
    configEdition
  ];
}
