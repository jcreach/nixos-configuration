{ config, lib, pkgs, ... }:

{
  # User configuration
  users.users.jcreach = {
    description = "Julien Creach";
    isNormalUser = true;
    createHome = true;
    initialPassword = "jcreach";
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "networkmanager" ];
  };
}
