{ config, lib, pkgs, ... }:
{
    users.users.jcreach = {
        description = "Julien Creach";
        isNormalUser = true;
        createHome = true;
        initialPassword = "jcreach";
        extraGroups = [ "wheel" "networkmanager" ];
    };

    home-manager.users.jcreach.home.file = {
        ".assets".source = ../../../.assets/images;
        ".face".source = ../../../.assets/images/avatar.png;
  };

}